var gulp        = require('gulp');
const { series } = require('gulp');
var browserSync = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./public"
        }
    });
});

function build(cb) {
    gulp.src(['src/**/*']).pipe(gulp.dest('public'));

    cb();
}

function serve(cb) {
  // place code for your default task here
    browserSync.init({
        server: {
            baseDir: "./public"
        }
    });

    cb();
}

exports.default = series(serve);;
